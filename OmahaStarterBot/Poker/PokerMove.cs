﻿namespace OmahaStarterBot.Poker
{
    /**
 * Class that represents the action of a bot.
 */
    public class PokerMove
    {
        public PokerMove(string botName, string act, int amt)
        {
            this.Player = botName;
            this.Action = act;
            this.Amount = amt;
        }

        public string Player { get; private set; }

        public string Action { get; private set; }

        public int Amount { get; private set; }

        /**
         * Returns a string representation of the move as a sentence of two words, being the action
         * string and the action amount. Returning the player name to the engine is not needed
         */
        public string MoveString()
        {
            return string.Format("{0} {1}", this.Action, this.Amount);
        }

    }
}
