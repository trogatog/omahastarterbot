﻿namespace OmahaStarterBot.Poker
{
    using System.Collections.Generic;

    public abstract class Hand
    {
        public Card GetCard(int index)
        {
            if (index >= 0 && index < this.Cards.Count)
                return this.Cards[index];
            return null;
        }

        public int GetNumberOfCards()
        {
            return this.Cards.Count;
        }

        public List<Card> Cards { get; protected set; }

        public string HandString()
        {
            string str = "[";
            for (int i = 0; i < this.Cards.Count - 1; i++)
                str += this.Cards[i].CardString() + ",";

            str += this.Cards[this.Cards.Count - 1].CardString() + "]";
            return str;
        }
    }
}
