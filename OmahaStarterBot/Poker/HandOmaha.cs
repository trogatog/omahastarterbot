﻿namespace OmahaStarterBot.Poker
{
    using System.Collections.Generic;

    public class HandOmaha : Hand
    {
        public HandOmaha(Card firstCard, Card secondCard, Card thirdCard, Card fourthCard)
        {
            Cards = new List<Card> { firstCard, secondCard, thirdCard, fourthCard };
        }
    }
}