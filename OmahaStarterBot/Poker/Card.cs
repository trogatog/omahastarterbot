﻿namespace OmahaStarterBot.Poker
{
    using System.Collections.Generic;
    using OmahaStarterBot.Enums;

    public class Card
    {
        private readonly CardHeight _height;
        private readonly CardSuit _suit;
        private readonly int _number;
        private static Dictionary<string, Card> stringToCard;


        /**
	 * Creates a card object based on a number between 0 and 51
	 */

        public Card(int num)
        {
            this._number = num;
            var findSuit = this._number/13;
            switch (findSuit)
            {
                case 0:
                    this._suit = CardSuit.Spades;
                    break;
                case 1:
                    this._suit = CardSuit.Hearts;
                    break;
                case 2:
                    this._suit = CardSuit.Clubs;
                    break;
                default:
                    this._suit = CardSuit.Diamonds;
                    break;
            }

            int findHeight = this._number%13;
            switch (findHeight)
            {
                case 0:
                    this._height = CardHeight.Deuce;
                    break;
                case 1:
                    this._height = CardHeight.Three;
                    break;
                case 2:
                    this._height = CardHeight.Four;
                    break;
                case 3:
                    this._height = CardHeight.Five;
                    break;
                case 4:
                    this._height = CardHeight.Six;
                    break;
                case 5:
                    this._height = CardHeight.Seven;
                    break;
                case 6:
                    this._height = CardHeight.Eight;
                    break;
                case 7:
                    this._height = CardHeight.Nine;
                    break;
                case 8:
                    this._height = CardHeight.Ten;
                    break;
                case 9:
                    this._height = CardHeight.Jack;
                    break;
                case 10:
                    this._height = CardHeight.Queen;
                    break;
                case 11:
                    this._height = CardHeight.King;
                    break;
                default:
                    this._height = CardHeight.Ace;
                    break;
            }
        }

        /**
	 * Returns the Card object that corresponds with the given card string. The first time this method is called, a
	 * map of all Cards corresponding with correct input strings is created.
	 * @param string : the string to be converted to a Card
	 */

        public static Card GetCard(string strCard)
        {
            if (stringToCard == null)
            {
                stringToCard = new Dictionary<string, Card>();
                for (var i = 0; i < 52; ++i)
                {
                    var card = new Card(i);
                    stringToCard.Add(card.CardString(), card);
                }
            }
            return stringToCard[strCard];
        }

        /**
	 * Returns the number of the card as a long.
	 */

        public long Number
        {
            get
            {
                int suitShift = this._number/13;
                int heightShift = this._number%13;
                return (1L << (16*suitShift + heightShift));
            }
        }

        /**
	 * Returns the height of this card.
	 */

        public CardHeight Height
        {
            get { return this._height; }
        }

        /**
	 * Returns the suit of this card.
	 */

        public CardSuit Suit
        {
            get { return this._suit; }
        }

        /**
	 * Returns a String representation of this card.
	 */

        public string CardString()
        {
            string str = null;
            int findHeight = this._number%13;
            switch (findHeight)
            {
                case 0:
                    str = "2";
                    break;
                case 1:
                    str = "3";
                    break;
                case 2:
                    str = "4";
                    break;
                case 3:
                    str = "5";
                    break;
                case 4:
                    str = "6";
                    break;
                case 5:
                    str = "7";
                    break;
                case 6:
                    str = "8";
                    break;
                case 7:
                    str = "9";
                    break;
                case 8:
                    str = "T";
                    break;
                case 9:
                    str = "J";
                    break;
                case 10:
                    str = "Q";
                    break;
                case 11:
                    str = "K";
                    break;
                case 12:
                    str = "A";
                    break;
            }
            int findSuit = this._number/13;
            switch (findSuit)
            {
                case 0:
                    str += "s";
                    break;
                case 1:
                    str += "h";
                    break;
                case 2:
                    str += "c";
                    break;
                default:
                    str += "d";
                    break;
            }

            return str;
        }
    }
}