﻿namespace OmahaStarterBot.Bot
{
    using System;
    using System.Linq;

    using OmahaStarterBot.Enums;
    using OmahaStarterBot.Poker;

    public class BotStarter : IBot
    {
        /**
	 * Implement this method to return the best move you can. Currently it will return a raise the average ordinal value 
	 * of your hand is higher than 9, a call when the average ordinal value is at least 6 and a check otherwise.
	 * As you can see, it will only consider it's current hand, not what's on the table.
	 * @param state : The current state of your bot, with all the (parsed) information given by the engine
	 * @param timeOut : The time you have to return a move
	 * @return PokerMove : The move you will be doing
	 */

        /**
	 * Quite a tedious method to check what we have in our hand. With 5 cards on the table we do 60(!) checks: all possible
	 * combinations of 2 out of 4 cards (our hand) times all possible combinations of 3 out of 5 cards (the table).
	 * For less cards on the table we need less calculation. This uses the com.stevebrecher package to see hand strength.
	 * @param hand : cards in hand
	 * @param table : cards on table
	 * @return HandCategory with what the bot has got, given the table and hand
	 */

        #region Public Methods and Operators
        public HandCategory GetHandCategory(BotState state, HandOmaha hand)
        {
            return StarterHandEval.Evaluate(state, hand);
        }

        public PokerMove GetMove(BotState state, long timeOut)
        {
            HandOmaha hand = state.Hand;
            var handCategory = GetHandCategory(state, hand);
            Console.Error.WriteLine(
                "my hand is {0}, opponent action is {1}, pot: {2}",
                handCategory,
                state.OpponentAction != null ? state.OpponentAction.MoveString() : "No Move", state.Pot);

            // Get the ordinal values of the cards in your hand
            int[] ordinalHand =
                {
                    (int)hand.GetCard(0).Height, (int)hand.GetCard(1).Height, (int)hand.GetCard(2).Height,
                    (int)hand.GetCard(3).Height
                };

            // Get the average ordinal value
            double averageOrdinalValue = ordinalHand.Aggregate<int, double>(0, (current, t) => current + t);

            averageOrdinalValue /= ordinalHand.Length;

            // Return the appropriate move according to our amazing strategy
            if (averageOrdinalValue >= 9)
            {
                return new PokerMove(state.MyName, "raise", 2 * state.BigBlind);
            }
            if (averageOrdinalValue >= 5)
            {
                return new PokerMove(state.MyName, "call", state.AmountToCall);
            }
            return new PokerMove(state.MyName, "check", 0);
        }
        #endregion
    }
}