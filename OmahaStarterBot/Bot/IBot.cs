﻿namespace OmahaStarterBot.Bot
{
    using OmahaStarterBot.Poker;

    public interface IBot
    {
        PokerMove GetMove(BotState state, long timeOut);
    }
}
