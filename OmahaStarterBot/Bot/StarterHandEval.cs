﻿namespace OmahaStarterBot.Bot
{
    using System.Linq;

    using OmahaStarterBot.Enums;
    using OmahaStarterBot.Poker;

    public static class StarterHandEval
    {
        public static HandCategory Evaluate(BotState state, HandOmaha hand)
        {
            return CheckRoyalFlush(state, hand);
        }

        private static HandCategory CheckRoyalFlush(BotState state, HandOmaha hand)
        {
            return CheckStraightFlush(state, hand);
        }

        private static HandCategory CheckStraightFlush(BotState state, HandOmaha hand)
        {
            return CheckFourOfAKind(state, hand);
        }

        private static HandCategory CheckFourOfAKind(BotState state, HandOmaha hand)
        {
            return CheckFullHouse(state, hand);
        }

        private static HandCategory CheckFullHouse(BotState state, HandOmaha hand)
        {
            return CheckFlush(state, hand);
        }

        private static HandCategory CheckFlush(BotState state, HandOmaha hand)
        {
            return CheckStraight(state, hand);
        }

        private static HandCategory CheckStraight(BotState state, HandOmaha hand)
        {
            return CheckThreeOfAKind(state, hand);
        }

        private static HandCategory CheckThreeOfAKind(BotState state, HandOmaha hand)
        {
            return CheckTwoPair(state, hand);
        }

        private static HandCategory CheckTwoPair(BotState state, HandOmaha hand)
        {
            return CheckPair(state, hand);
        }

        private static HandCategory CheckPair(BotState state, HandOmaha hand)
        {
            foreach (var card in hand.Cards)
            {
                foreach (var otherCard in hand.Cards.Where(c => c != card).Union(state.Table))
                {
                    if(card.Height == otherCard.Height)
                        return HandCategory.Pair;
                }
            }

            return HandCategory.NoPair;
        }
    }
}
