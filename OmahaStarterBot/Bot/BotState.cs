﻿namespace OmahaStarterBot.Bot
{
    using System;
    using System.Collections.Generic;

    using OmahaStarterBot.Poker;

    public class BotState
    {
        private int _timebank;
        private int _handsPerLevel;
        private int _timePerMove;

        private readonly Dictionary<string, string> _settings = new Dictionary<string, string>();

        public BotState()
        {
            this.MyName = string.Empty;
            this.Sidepots = new List<int>();
            this.Table = new List<Card>();
        }

        /**
         * Parses the settings for this game
         * @param key : key of the information given
         * @param value : value to be set for the key
         */

        public void UpdateSetting(string key, string value)
        {
            this._settings.Add(key, value);
            if (key.Equals("yourBot"))
            {
                this.MyName = value;
            }
            else if (key.Equals("timeBank"))
            {
                // Maximum amount of time your bot can take for one response
                this._timebank = int.Parse(value);
            }
            else if (key.Equals("timePerMove"))
            {
                // The extra amount of time you get per response
                this._timePerMove = int.Parse(value);
            }
            else if (key.Equals("handsPerLevel"))
            {
                // Number of rounds before the blinds are increased
                this._handsPerLevel = int.Parse(value);
            }
            else if (key.Equals("startingStack"))
            {
                // Starting stack for each bot
                this.MyStack = int.Parse(value);
                this.OpponentStack = int.Parse(value);
            }
            else
            {
                Console.Error.WriteLine("Unknown settings command: {0} {1}", key, value);
            }
        }

        /**
         * Parses the match information
         * @param key : key of the information given
         * @param value : value to be set for the key
         */

        public void UpdateMatch(string key, string value)
        {
            if (key.Equals("round"))
            {
                // Round number
                this.Round = int.Parse(value);
                Console.Error.WriteLine("Round " + this.Round); //printing the round to the output for debugging
                this.ResetRoundVariables();
            }
            else if (key.Equals("smallBlind"))
            {
                // Value of the small blind
                this.SmallBlind = int.Parse(value);
            }
            else if (key.Equals("bigBlind"))
            {
                // Value of the big blind
                this.BigBlind = int.Parse(value);
            }
            else if (key.Equals("onButton"))
            {
                // Which bot has the button, onButton is true if it's your bot
                this.OnButton = value.Equals(this.MyName);
            }
            else if (key.Equals("maxWinPot"))
            {
                // The size of the current pot
                this.Pot = int.Parse(value);
            }
            else if (key.Equals("amountToCall"))
            {
                // The amount of the call
                this.AmountToCall = int.Parse(value);
            }
            else if (key.Equals("table"))
            {
                // The cards on the table
                this.Table = this.ParseCards(value);
            }
            else
            {
                Console.Error.WriteLine("Unknown match command: {0} {1}", key, value);
            }
        }

        /**
         * Parses the information given about stacks, blinds and moves
         * @param bot : bot that this move belongs to (either you or the opponent)
         * @param key : key of the information given
         * @param amount : value to be set for the key
         */

        public void UpdateMove(string bot, string key, string amount)
        {
            if (bot.Equals(this.MyName))
            {
                if (key.Equals("stack"))
                {
                    // The amount in your starting stack
                    this.MyStack = int.Parse(amount);
                }
                else if (key.Equals("post"))
                {
                    // The amount you have to pay for the blind
                    this.MyStack -= int.Parse(amount);
                }
                else if (key.Equals("hand"))
                {
                    // Your cards
                    var cards = this.ParseCards(amount);
                    this.Hand = new HandOmaha(cards[0], cards[1], cards[2], cards[3]);
                }
                else if (key.Equals("wins"))
                {
                    // Your winnings, not stored
                }
            }
            else
            {
                // assume it's the opponent
                if (key.Equals("stack"))
                {
                    // The amount in your opponent's starting stack
                    this.OpponentStack = int.Parse(amount);
                }
                else if (key.Equals("post"))
                {
                    // The amount your opponent paid for the blind
                    this.OpponentStack -= int.Parse(amount);
                }
                else if (key.Equals("hand"))
                {
                    // Hand of the opponent on a showdown, not stored
                }
                else if (key.Equals("wins"))
                {
                    // Opponent winnings, not stored
                }
                else
                {
                    // The move your opponent did
                    this.OpponentAction = new PokerMove(bot, key, int.Parse(amount));
                }
            }
        }

        /**
         * Parse the input string from the engine to actual Card objects
         * @param String value : input
         * @return Card[] : array of Card objects
         */

        private List<Card> ParseCards(string value)
        {
            if (value.EndsWith("]"))
            {
                value = value.Substring(0, value.Length - 1);
            }
            if (value.StartsWith("["))
            {
                value = value.Substring(1);
            }
            if (value.Length == 0)
            {
                return new List<Card>();
            }
            var parts = value.Split(',');
            var cards = new List<Card>();
            foreach (var part in parts)
            {
                cards.Add(Card.GetCard(part));
            }
            return cards;
        }

        /**
         * Reset all the variables at the start of the round,
         * just to make sure we don't use old values
         */

        private void ResetRoundVariables()
        {
            this.SmallBlind = 0;
            this.BigBlind = 0;
            this.Pot = 0;
            this.OpponentAction = null;
            this.AmountToCall = 0;
            this.Hand = null;
            this.Table = new List<Card>();
        }

        public int Round { get; private set; }

        public int SmallBlind { get; private set; }

        public int BigBlind { get; private set; }

        public bool OnButton { get; private set; }

        public int MyStack { get; private set; }

        public int OpponentStack { get; private set; }

        public int Pot { get; private set; }

        public PokerMove OpponentAction { get; private set; }

        public int CurrentBet { get; private set; }

        public HandOmaha Hand { get; private set; }

        public List<Card> Table { get; private set; }

        public string GetSetting(string key)
        {
            return this._settings[key];
        }

        public List<int> Sidepots { get; private set; }

        public string MyName { get; private set; }

        public int AmountToCall { get; private set; }

    }
}
