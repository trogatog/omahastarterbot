﻿namespace OmahaStarterBot
{
    using OmahaStarterBot.Bot;

    class Program
    {
        static void Main(string[] args)
        {
            var parser = new BotParser(new BotStarter());
            parser.Run();
        }
    }
}
