﻿namespace OmahaStarterBot.Enums
{
    public enum HandCategory
    {
        NoPair,
        Pair,
        TwoPair,
        ThreeOfAKind,
        Straight,
        Flush,
        FullHouse,
        FourOfAKind,
        StraightFlush,
        RoyalFlush
    }
}
