﻿namespace OmahaStarterBot.Enums
{
    public enum CardSuit
    {
        Spades, 
        Hearts, 
        Clubs,
        Diamonds
    }
}
